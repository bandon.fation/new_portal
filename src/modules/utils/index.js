export const wait_time = async (_t = 1) => {
  return new Promise((rs, rj) => {
    setTimeout(() => {
      rs();
    }, _t * 1000);
  });
}


export const split_long_num = (num) => {
    if(num === null) return '';
    let num_str = num + '';
    let [int_num, float_num = ''] = num_str.split('.');
    let num_arr = int_num.split('');
    let n = 0, n_arr = []; 
    for(let i of num_arr.reverse()){
        if(!(n%3) && n){n_arr.push(',');}
        n_arr.push(i);
        n ++;
    };
    return n_arr.reverse().join('') + (float_num ? '.' + float_num : '');
}