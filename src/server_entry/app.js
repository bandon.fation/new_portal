import { createApp } from './controller/app_ssr.js'

export default context => {
  const { app } = createApp()
  return app
}