import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// route-level code splitting

import index_view from '../views/index.vue'
import wallet from '../views/wallet.vue'
// import scan_qr_code from '../views/scan_qr_code.vue'

export function createRouter (url) {
  let router = new Router({
    mode: 'history',
    fallback: false,
    scrollBehavior: () => ({ y: 0 }),
    routes: [
      // { path: '/scan_qr_code/:page(\\d+)?', component: scan_qr_code, name: 'scan_qr_code' },
      { path: '/wallet/:lang?', component: wallet, name: 'wallet' },
      { path: '/index/:lang?', component: index_view, name: 'app' },
      { path: '/', redirect: '/index' }
    ]
  });
  return router;
}

import Vuex from 'vuex'
Vue.use(Vuex)

export const get_lang_key = () => {

}

export function createStore () {
  return new Vuex.Store({
    state: {},
  })
}

