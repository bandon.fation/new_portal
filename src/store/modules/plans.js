
import {
  query_plans as QUERY_PLANS,
  query_news_by_id,
} from '@/service'

import {
  NEWS_STATIC_HOST as plans_static_host
} from '@/service/index.js'

const OPTIONS = {
  UPDATE_PLANS: 'UPDATE_PLANS',

  CLEAR_PLANS: 'CLEAR_PLANS',

  UPDATE_PLANS_PAGE:  'UPDATE_PLANS_PAGE',

  UPDATE_PLANS_TOTAL: 'UPDATE_PLANS_TOTAL',

  UPDATE_PLANS_LIMIT: 'UPDATE_PLANS_LIMIT',

  UPDATE_PLANS_LOAD_STAUS: 'UPDATE_PLANS_LOAD_STAUS',

  UPDATE_IMG_URL: 'update_img_url',

  START_LOAD: 'start_load',

  END_LOAD: 'end_load',

  UPDATE_PLANS_INFO: 'UPDATE_PLANS_INFO',

  LOAD_PLANS: 'LOAD_PLANS',

  LOAD_FIRST_PAGE_PLANS: 'LOAD_FIRST_PAGE_PLANS',

  LOAD_NEXT_PAGE_PLANS: 'LOAD_NEXT_PAGE_PLANS',

  LOAD_PLANS_BY_ID: 'LOAD_PLANS_BY_ID',

  GET_PLANS: 'GET_PLANS'
}

const initState = {
  plans: [],
  page: 1,
  limit: 10,
  total: 0,
  more: true,
  on_load: true,
  error_msg: ''
}

const mutations = {
  [OPTIONS.UPDATE_PLANS] (state, plans) {
    state.plans.splice(state.plans.length, 0, ...plans);
    
  },
  [OPTIONS.CLEAR_PLANS] (state) {
    state.page = 1;
    state.total = 0;
    state.plans.splice(0, state.plans.length);
  },
  [OPTIONS.UPDATE_IMG_URL] (state) {
    let data = state.plans;
    for(let plans_item of data){
      let val = [];
      try{
        val = JSON.parse( plans_item.description )
      }catch(e){
        val = [
           {'insert': plans_item.description}
         ]
      }

      if(!val.splice){
        val = [
           {'insert': plans_item.description}
         ]
      }

      for(let row of val){
        if(row.insert && row.insert.image){
          let is_url = row.insert.image.indexOf('http://') == 0 || row.insert.image.indexOf('httpw://') == 0
          if(!is_url){
            row.insert.image = `${plans_static_host}${row.insert.image}`;
          }
        }
      }

      plans_item.description = JSON.stringify(val);
    }

    for(let row of data){
      row.sliders.forEach((i, index) => {
        let base_file_name = i.split('/static/')[1];
        if(base_file_name){
          row.sliders[index] = `${ plans_static_host }/static/${ base_file_name }`
        }
      });
    }
  },
  [OPTIONS.UPDATE_PLANS_LIMIT] (state, limit) {
    state.limit = limit
  },
  [OPTIONS.UPDATE_PLANS_TOTAL] (state, total) {
    state.total = total;
  },
  [OPTIONS.UPDATE_PLANS_PAGE] (state, page) {
    state.page = page;
  },
  [OPTIONS.UPDATE_PLANS_LOAD_STAUS] (state, status = false) {
    state.on_load = status;
  },
  [OPTIONS.START_LOAD] (state) {
    state.on_load = true;
  },
  [OPTIONS.END_LOAD] (state) {
    state.on_load = false;
  },
  [OPTIONS.UPDATE_PLANS_INFO] (state, {page, limit, total}) {
    state.page = page;
    state.limit = limit;
    state.total = total;
    if(state.page >= total) {
      state.more = false;
    }else{
      state.more = true;
    }
  }
}

const actions = {
  async [OPTIONS.LOAD_PLANS] ({state, commit}, params = {tags: []}) {
    commit(OPTIONS.START_LOAD);

    let {is_error, data: base_data} = await QUERY_PLANS({page: state.page, tags: params.tags});
    if(is_error){
      commit(OPTIONS.END_LOAD);
      return ;
    }
    const {page, limit, total, data} = base_data;
    // const plans_static_host = 'http://127.0.0.1:1213'

    commit(OPTIONS.UPDATE_PLANS, data);
    commit(OPTIONS.UPDATE_IMG_URL);
    commit(OPTIONS.UPDATE_PLANS_INFO, {page, limit, total});

    commit(OPTIONS.END_LOAD);
    return data;
  },
  async [OPTIONS.LOAD_NEXT_PAGE_PLANS] ({state, commit, dispatch}, params = {tags: []}) {
    let page  = state.page,
        total = state.total;

    if(!state.more) return;

    if(page >= total) return;

    let next_page = page + 1;

    commit(OPTIONS.UPDATE_PLANS_PAGE, next_page);
    await dispatch(OPTIONS.LOAD_PLANS, params);

  },
  async [OPTIONS.LOAD_FIRST_PAGE_PLANS] ({state, commit, dispatch}, params = {tags: []}) {
    commit(OPTIONS.CLEAR_PLANS);
    console.log('LOAD_FIRST_PAGE_PLANS')
    console.log(params)
    await dispatch(OPTIONS.LOAD_PLANS, params);
  },
  async [OPTIONS.LOAD_PLANS_BY_ID] ({state, commit, dispatch}, plans_id) {
    let {is_error, data: base_data} = await query_news_by_id({id: plans_id});
    commit(OPTIONS.CLEAR_PLANS);
    commit(OPTIONS.UPDATE_PLANS, base_data);
    commit(OPTIONS.UPDATE_IMG_URL);
  }
}

const getters = {

}

export default {
    state: initState,
    mutations,
    actions,
    getters,
};